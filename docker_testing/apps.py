from django.apps import AppConfig


class DockerTestingConfig(AppConfig):
    name = 'docker_testing'
